package com.yicai.springcloud.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author Mga
 * @Date 2023/7/27 19:53
 * @注释
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Storage {
    private Long id;
    private Long productId;
    private Integer total;
    private Integer used;
    private Integer residue;
}
