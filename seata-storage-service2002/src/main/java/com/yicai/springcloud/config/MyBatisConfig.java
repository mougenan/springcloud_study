package com.yicai.springcloud.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Author Mga
 * @Date 2023/7/27 0:23
 * @注释
 */
@Configuration
@MapperScan({"com.yicai.springcloud.dao"})
public class MyBatisConfig {
}
