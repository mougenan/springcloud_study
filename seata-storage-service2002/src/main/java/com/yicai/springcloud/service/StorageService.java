package com.yicai.springcloud.service;

import org.apache.ibatis.annotations.Param;

/**
 * @Author Mga
 * @Date 2023/7/27 20:06
 * @注释
 */

public interface StorageService {
    void decrease(Long productId, Integer count);
}
