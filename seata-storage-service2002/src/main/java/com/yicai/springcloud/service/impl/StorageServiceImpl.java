package com.yicai.springcloud.service.impl;

import com.yicai.springcloud.dao.StorageDao;
import com.yicai.springcloud.service.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * @Author Mga
 * @Date 2023/7/27 20:08
 * @注释
 */
@Service
public class StorageServiceImpl implements StorageService {

    private static final Logger LOGGER= LoggerFactory.getLogger(StorageServiceImpl.class);

    @Resource
    private StorageDao storageDao;

    @Override
    public void decrease(Long productId, Integer count) {
        LOGGER.info("--------->storage-service中扣减库存开始");
        storageDao.decrease(productId,count);
        LOGGER.info("------------->storage-service中扣减库存结束");
    }
}
