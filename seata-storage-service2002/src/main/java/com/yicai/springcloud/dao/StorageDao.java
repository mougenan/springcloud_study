package com.yicai.springcloud.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author Mga
 * @Date 2023/7/27 19:58
 * @注释
 */
@Mapper
public interface StorageDao {
    void decrease(@Param("productId")Long productId,@Param("count")Integer count);
}
