package com.yicai.springcloud.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author Mga
 * @Date 2023/7/3 21:48
 * @注释
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Payment implements Serializable {  //序列化
    private Long id;
    private String serial;

}
