package com.yicai.springcloud.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author Mga
 * @Date 2023/7/3 21:50
 * @注释
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
//前端返回的数据对象封装实体
public class CommonResult<T>{

    private Integer code;
    private String message;
    private T data;


    public CommonResult(Integer code, String message){
        this(code,message,null);
    }
}
