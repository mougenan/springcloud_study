package com.yicai.springcloud.service;

import org.springframework.stereotype.Component;

/**
 * @Author Mga
 * @Date 2023/7/8 22:27
 * @注释
 */
@Component
public class PaymentFallbackService implements PaymentHystrixService{
    @Override
    public String paymentInfo_ok(Integer id) {
        return "- ----PaymentFallbackservice fall back-paymentInfo_oK ,o(T—T)o\n";
    }

    @Override
    public String paymentInfo_Timeout(Integer id) {
        return "-----PaymentFallbackservice fall paymentInfo_Timeout ,o(T-)口\n";
    }
}
