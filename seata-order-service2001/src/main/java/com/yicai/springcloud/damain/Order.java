package com.yicai.springcloud.damain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @Author Mga
 * @Date 2023/7/26 23:45
 * @注释
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Order {
    private Long id;
    private Long userId;
    private Long productId;
    private BigDecimal money;
    private Integer status;
    private Integer count;
}
