package com.yicai.springcloud.service.impl;

import com.yicai.springcloud.damain.Order;
import com.yicai.springcloud.dao.OrderDao;
import com.yicai.springcloud.service.AccountService;
import com.yicai.springcloud.service.OrderService;
import com.yicai.springcloud.service.StorageService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Author Mga
 * @Date 2023/7/27 0:04
 * @注释
 */
@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderDao orderDao;
    @Resource
    private StorageService storageService;
    @Resource
    private AccountService accountService;

    @Override
    @GlobalTransactional(name = "fsp-create-order",rollbackFor = Exception.class)
    public void create(Order order) {
        log.info("---------->开始新建订单");
        orderDao.create(order);
        log.info("---------->订单微服务开始调用库存，做扣减count");
        storageService.decrease(order.getProductId(),order.getCount());
        log.info("---------->订单微服务开始调用库存,做扣减end");
        log.info("---------->订单微服务开始调用账户，做扣减money");
        accountService.decrease(order.getUserId(),order.getMoney());
        log.info("---------->订单微服务开始调用账户，做扣减end");
        log.info("---------->修改订单状态开始");
        orderDao.update(order.getUserId(),0);
        log.info("---------->修改订单状态结束");
        log.info("---------->下订单结束了！");
    }
}
