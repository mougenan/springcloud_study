package com.yicai.springcloud.service;

import com.yicai.springcloud.damain.Order;

/**
 * @Author Mga
 * @Date 2023/7/27 0:02
 * @注释
 */

public interface OrderService {
    void create(Order order);
}
