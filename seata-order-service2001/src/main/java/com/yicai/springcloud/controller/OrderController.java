package com.yicai.springcloud.controller;

import com.yicai.springcloud.damain.CommonResult;
import com.yicai.springcloud.damain.Order;
import com.yicai.springcloud.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author Mga
 * @Date 2023/7/27 0:20
 * @注释
 */
@RestController
@Slf4j
public class OrderController {

    @Resource
    public OrderService orderService;

    @GetMapping("/order/create")
    public CommonResult create(Order order){
        System.out.println(order.toString());
        orderService.create(order);
        return new CommonResult(200,"订单创建成功");
    }
}
