package com.yicai.springcloud.dao;

import com.yicai.springcloud.damain.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author Mga
 * @Date 2023/7/26 23:47
 * @注释
 */
@Mapper
public interface OrderDao {
    void create(Order order);
    void update(@Param("userId") Long userId,@Param("status")Integer status);
}
