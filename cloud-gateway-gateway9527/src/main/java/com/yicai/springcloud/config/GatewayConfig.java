package com.yicai.springcloud.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author Mga
 * @Date 2023/7/9 10:15
 * @注释
 */
@Configuration
public class GatewayConfig {
    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder routeLocatorBuilder){
        RouteLocatorBuilder.Builder builder=routeLocatorBuilder.routes();
        builder.route("path_route_yicai",
                r->r.path("/qq_53957101/article/details/130376180")
                        .uri("https://blog.csdn.net/qq_53957101/article/details/130376180")).build();
        return builder.build();
    }
}
