package com.yicai.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @Author Mga
 * @Date 2023/7/13 18:59
 * @注释
 */
@SpringBootApplication
@EnableEurekaClient
public class ConsumerMQMain8803 {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerMQMain8803.class,args);
    }
}
