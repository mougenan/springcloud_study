package com.yicai.springcloud.myhandler;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.yicai.springcloud.entities.CommonResult;
import com.yicai.springcloud.entities.Payment;

/**
 * @Author Mga
 * @Date 2023/7/25 19:21
 * @注释
 */

public class CustomerBlockHandler {
    public static CommonResult handlerException1(BlockException exception){
        return new CommonResult(4444,"按客户自定义限流测试异常处理1,全局异常处理一");
    }
    public static CommonResult handlerException2(BlockException exception){
        return new CommonResult(4444,"按客户自定义限流测试异常处理2，全局异常处理二");
    }
}
