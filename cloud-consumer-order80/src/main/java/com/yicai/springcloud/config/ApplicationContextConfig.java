package com.yicai.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @Author Mga
 * @Date 2023/7/4 19:24
 * @注释
 */
@Configuration
public class ApplicationContextConfig {

    @Bean
//    @LoadBalanced   //使用自己定义的负载均衡算法
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }

}
