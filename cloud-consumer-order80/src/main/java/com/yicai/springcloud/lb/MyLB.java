package com.yicai.springcloud.lb;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author Mga
 * @Date 2023/7/8 14:04
 * @注释
 */
@Component
public class MyLB implements LoadBalancer{

    private AtomicInteger atomicInteger=new AtomicInteger(0);//原子类

    public final int getAndIncrement(){
        int current;
        int next;
        do{
            current=this.atomicInteger.get();
            next=current>=2147483647?0:current+1; //2147483647 int最大值，防止next溢出
        }while (!this.atomicInteger.compareAndSet(current,next));
        System.out.println("********次数next:"+next);
        return next;
    }
    @Override
    public ServiceInstance instance(List<ServiceInstance> serviceInstances) {
        int index = getAndIncrement()%serviceInstances.size();
        return serviceInstances.get(index);
    }
}
