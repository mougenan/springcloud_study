package com.yicai.springcloud.lb;

import org.springframework.cloud.client.ServiceInstance;

import java.util.List;

/**
 * @Author Mga
 * @Date 2023/7/8 14:02
 * @注释
 */

public interface LoadBalancer {
    //每一个服务实例
    ServiceInstance instance(List<ServiceInstance> serviceInstances);
}
