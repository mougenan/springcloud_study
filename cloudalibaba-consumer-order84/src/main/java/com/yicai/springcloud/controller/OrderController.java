package com.yicai.springcloud.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.yicai.springcloud.entities.CommonResult;
import com.yicai.springcloud.entities.Payment;
import com.yicai.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @Author Mga
 * @Date 2023/7/25 20:08
 * @注释
 */
@RestController
@Slf4j
public class OrderController {
    public static final String SERVICE_URL = "http://nacos-payment-provider";

    @Resource
    private RestTemplate restTemplate;

    @RequestMapping("/consumer/fallback/{id}")
    @SentinelResource(value = "fallback",fallback = "handlerFallback",
            blockHandler = "blockHandler",exceptionsToIgnore = {IllegalArgumentException.class})
//    @SentinelResource(value = "fallback",blockHandler = "blockHandler")
    public CommonResult<Payment> fallback(@PathVariable Long id){
        CommonResult<Payment> result=restTemplate.getForObject(SERVICE_URL+"/paymentSQL/"+id,CommonResult.class,id);
        if(id==4){
            throw new IllegalArgumentException("IllegalArgumentException,非法参数异常...");
        }else if(result.getData()==null){
            throw new NullPointerException("NullPointerException,该ID没有对应记录,空指针异常...");
        }
        return result;
    }

    //blockHandler
    public CommonResult blockHandler(@PathVariable Long id, BlockException blockException){
        Payment payment=new Payment(id,"null");
        return new CommonResult(444,"sentinel限流兜底blockHandler,exception内容"+blockException.getMessage(),payment);
    }

    //fallback
    public CommonResult handlerFallback(@PathVariable Long id,Throwable e){
        Payment payment=new Payment(id,"null");
        return new CommonResult(444,"兜底异常handlerFallback,exception内容"+e.getMessage(),payment);
    }

    @Resource
    private PaymentService paymentService;

    @GetMapping(value = "/consumer/paymentSQL/{id}")
    public CommonResult<Payment> paymentSQL(@PathVariable("id") Long id){
        return paymentService.paymentSQL(id);
    }
}
