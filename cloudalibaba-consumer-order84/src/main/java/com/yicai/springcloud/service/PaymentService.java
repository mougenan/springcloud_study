package com.yicai.springcloud.service;

import com.yicai.springcloud.entities.CommonResult;
import com.yicai.springcloud.entities.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @Author Mga
 * @Date 2023/7/25 21:11
 * @注释
 */
@FeignClient(value = "nacos-payment-provider",
fallback = PaymentFallbackService.class)
public interface PaymentService {
    @GetMapping(value = "/paymentSQL/{id}")
    CommonResult<Payment> paymentSQL(@PathVariable("id") Long id);
}
