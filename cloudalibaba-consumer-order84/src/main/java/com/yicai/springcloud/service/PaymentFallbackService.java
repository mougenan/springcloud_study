package com.yicai.springcloud.service;

import com.yicai.springcloud.entities.CommonResult;
import com.yicai.springcloud.entities.Payment;
import org.springframework.stereotype.Component;

/**
 * @Author Mga
 * @Date 2023/7/25 21:14
 * @注释
 */
@Component
public class PaymentFallbackService implements PaymentService{
    @Override
    public CommonResult<Payment> paymentSQL(Long id) {
        return new CommonResult<>(444,"服务降级返回，-----paymentfallbackservice",new Payment(id,"error"));
    }
}
