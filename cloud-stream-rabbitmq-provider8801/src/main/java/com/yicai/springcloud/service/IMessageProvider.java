package com.yicai.springcloud.service;

/**
 * @Author Mga
 * @Date 2023/7/12 22:24
 * @注释
 */

public interface IMessageProvider {
    String send();
}
