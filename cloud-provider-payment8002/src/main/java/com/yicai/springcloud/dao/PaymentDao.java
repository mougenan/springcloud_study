package com.yicai.springcloud.dao;

import com.yicai.springcloud.entities.Payment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author Mga
 * @Date 2023/7/3 21:54
 * @注释
 */
@Mapper
public interface PaymentDao {
    //新增
    int create(Payment payment);
    //读取
    Payment getPaymentById(@Param("id") Long id);
}
