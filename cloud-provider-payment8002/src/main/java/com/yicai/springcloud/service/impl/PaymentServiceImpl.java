package com.yicai.springcloud.service.impl;

import com.yicai.springcloud.dao.PaymentDao;
import com.yicai.springcloud.entities.Payment;
import com.yicai.springcloud.service.PaymentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Author Mga
 * @Date 2023/7/3 22:06
 * @注释
 */
@Service
public class PaymentServiceImpl implements PaymentService {

    @Resource
    private PaymentDao paymentDao;

    @Override
    public int create(Payment payment) {
        return paymentDao.create(payment);
    }

    @Override
    public Payment getPaymentById(Long id) {
        return paymentDao.getPaymentById(id);
    }
}
