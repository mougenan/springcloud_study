package com.yicai.springcloud.service;

import com.yicai.springcloud.entities.Payment;
import org.apache.ibatis.annotations.Param;

/**
 * @Author Mga
 * @Date 2023/7/3 22:05
 * @注释
 */

public interface PaymentService {
    //新增
    int create(Payment payment);
    //读取
    Payment getPaymentById(@Param("id") Long id);
}
