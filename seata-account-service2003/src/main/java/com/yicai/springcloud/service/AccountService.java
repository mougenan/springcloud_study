package com.yicai.springcloud.service;

import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * @Author Mga
 * @Date 2023/7/27 20:28
 * @注释
 */

public interface AccountService {
    void decrease(Long userId, BigDecimal money);
}
