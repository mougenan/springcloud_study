package com.yicai.springcloud.service.impl;

import com.yicai.springcloud.dao.AccountDao;
import com.yicai.springcloud.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

/**
 * @Author Mga
 * @Date 2023/7/27 20:29
 * @注释
 */
@Service
public class AccountServiceImpl implements AccountService {

    private static final Logger LOGGER= LoggerFactory.getLogger(AccountServiceImpl.class);

    @Resource
    private AccountDao accountDao;

    @Override
    public void decrease(Long userId, BigDecimal money) {
        LOGGER.info("--------->account-service中扣减库存开始");
        try{
            TimeUnit.SECONDS.sleep(20000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        accountDao.decrease(userId,money);
        LOGGER.info("------------->account-service中扣减库存结束");
    }
}
