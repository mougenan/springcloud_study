package com.yicai.springcloud.controller;

import com.yicai.springcloud.entities.CommonResult;
import com.yicai.springcloud.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * @Author Mga
 * @Date 2023/7/27 20:31
 * @注释
 */
@RestController
@Slf4j
public class AccountController {

    @Resource
    AccountService accountService;

    @PostMapping(value = "/account/decrease")
    public CommonResult decrease(@RequestParam("userId") Long userId, @RequestParam("money") BigDecimal money){
        accountService.decrease(userId,money);
        return new CommonResult(200,"扣减账余额成功!");
    }
}
